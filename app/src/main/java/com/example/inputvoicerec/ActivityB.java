package com.example.inputvoicerec;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

// Set visibility to invisible for RadioButtons before you show the app

public class ActivityB extends Activity implements TextWatcher { //, TextView.OnEditorActionListener {

    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private boolean isRecording = false;
    private int bufferSize = 0;
    private int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    private int BytesPerElement = 2; // 2 bytes in 16bit format
    private int VoiceNrExced = 0;
    private int j = 0;
    private int frameMultiplyer = 1;   // For Counting down seconds
    private int NrFramesSilence = 172; // approximately 3 seconds
    private boolean automatics = true; //After voice analysis is finished this is false
    private int seconds = 0; // Seconds of silence for decrementing
    private int NrFramesIn1Sec = (int)Math.ceil(RECORDER_SAMPLERATE/BufferElements2Rec); // 44 if fps = 44100
    private int inputSeconds = 3; // input seconds, 3 = default
    private String recPath = Environment.getExternalStorageDirectory() + "/audiorecordtest.mp4";
    String filePathAudioRec = Environment.getExternalStorageDirectory().getPath() + "/voice8K16bitmono.pcm";
    private int silenceThreshold_1 = 350, silenceThreshold_2 = 1000, silenceThreshold_3 = 2000;

    private String savedText = null;
    private MediaRecorder mediaRecorder = null;
    private AudioRecord recorder = null;
    private Thread recordingThread = null;
    private EditText editText = null;
    private Handler handler = null;
    private ImageView p20_0= null, p20_1= null, p20_2= null, p20_3 = null, hal9000_0n=null, hal9000_0ff=null;
    private RadioGroup radioGroup = null;
    private RadioButton rb1 = null, rb2 = null;
    private TextView textView = null, txtViewSecCntdown = null;
    private ActionBar actionBar = null;
    private View v1=null,v2=null,v3=null,v4=null;
    private RadioButtonsSound selection = RadioButtonsSound.SOUND_UNDECLARED;


    //fields only for the ENCODER
    public static final String AUDIO_RECORDING_FILE_NAME = "recording.raw"; // Input PCM file
    public static final String COMPRESSED_AUDIO_FILE_NAME = "compressed.mp4"; // Output MP4 file
    public static final String COMPRESSED_AUDIO_FILE_MIME_TYPE = "audio/mp4a-latm";
    public static final int COMPRESSED_AUDIO_FILE_BIT_RATE = 128000; // 128kbps
    public static final int SAMPLING_RATE = 44100;
    public static final int CODEC_TIMEOUT_IN_MS = 5000; //ms
    public static final int BUFFER_SIZE = 88200; // 44100 *2 , 2 bytes(16bit) 44100 times per second = 88200 bytes
    private static final String LOGTAG = "SEBA";
    private boolean mStop = false; // added myself -> ??, maybe manual stop

    String filePath = "/storage/emulated/0/voice8K16bitmono.pcm";
//    String filePath = Environment.getExternalStorageDirectory().getPath() + "/" + AUDIO_RECORDING_FILE_NAME;
    MediaMuxer mux = null;
    MediaFormat outputFormat = null;
    MediaCodec codec = null;
    ByteBuffer[] codecInputBuffers = null;
    ByteBuffer[] codecOutputBuffers = null;
    MediaCodec.BufferInfo outBuffInfo = null;
    File inputFile = null;
    FileInputStream fis = null;
    File outputFile = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);

        //check if onCreate() is run for the first time
        if(savedInstanceState != null)
        {
            Toast toast = null;
            toast = Toast.makeText(this, R.string.toast_oncreate_second, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP,0,0);
            toast.show();
        }

        editText = (EditText) findViewById(R.id.editText);
//        editText.requestFocus();
//        editText.setOnEditorActionListener(this);
        editText.addTextChangedListener(this);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroupId);
        rb1 = (RadioButton) findViewById(R.id.radioButtonSound);
        rb2 = (RadioButton) findViewById(R.id.radioButtonNoSound);
        textView = (TextView) findViewById(R.id.textView);
//        txtViewSecCntdown = (TextView) findViewById(R.id.textViewSecCountdown);


        actionBar = getActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v1 = inflator.inflate(R.layout.custom_imageview1, null);
        v2 = inflator.inflate(R.layout.custom_imageview2, null);
        v3 = inflator.inflate(R.layout.custom_imageview3, null);
        v4 = inflator.inflate(R.layout.custom_imageview4, null);

        seconds = inputSeconds;

        actionBar.setCustomView(v1);

        handler = new Handler();

        bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);

        recThread();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_activity_b, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.action_exitTxt:
//                Toast.makeText(this, "Exit with text was clicked",Toast.LENGTH_SHORT).show();
                exitActivity();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();

        stopRecording();
        Log.d("SEBA", "Stopping from inside onPause");
//        delFile(filePathAudioRec);
        finish();

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
//        Toast.makeText(this,"after change",Toast.LENGTH_SHORT).show();
//        txtViewSecCntdown.setVisibility(View.INVISIBLE);
        savedText = editText.getText().toString();
        if(automatics) {
            automatics = false;
            radioGroup.setVisibility(View.VISIBLE);
            textView.setText(R.string.textview_quit);
        }
    }

    @Override
    public void onBackPressed() {
        exitActivity();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
        {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }



//    record using MediaRecorder
//    private void RecordAAC() {
//        if(mediaRecorder != null)
//        {
//            Log.d("SEBA","mediaRecorder != null in RecordAAC()");
//            return;
//        }
//
//        mediaRecorder = new MediaRecorder();
//        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
//        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
////                mediaRecorder.setAudioEncoder(MediaRecorder.getAudioSourceMax());
//        mediaRecorder.setAudioEncodingBitRate(16);
//        mediaRecorder.setAudioSamplingRate(44100);
//        mediaRecorder.setOutputFile(recPath);
//        try {
//            mediaRecorder.prepare();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        mediaRecorder.start();
//
////        Toast.makeText(this,"Recording started ...",Toast.LENGTH_SHORT).show();
//    }

    private void recThread(){

        recordingThread = new Thread(new Runnable() {
            @Override
            public void run() {
//                RecordAAC();
                voiceDetection();
            }
        }, "AudioRecorder Thread");
        recordingThread.start();
    }

    private void voiceDetection(){
        if (recorder != null)
        {
            Log.d("SEBA","mediaRecorder != null in voiceDetection()");
            return;
        }

        short sData[] = new short[BufferElements2Rec];

        // Recorder for Automatic audio stop
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDER_SAMPLERATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize);

        recorder.startRecording();
        isRecording = true;

//        String filePathAudioRec = Environment.getExternalStorageDirectory().getPath() + "/voice8K16bitmono.pcm";

        FileOutputStream os = null;
        try {
            os = new FileOutputStream(filePathAudioRec);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        initPcmToAac();

        while (isRecording) {
            // gets the voice output from microphone to byte format

            recorder.read(sData, 0, BufferElements2Rec);
            System.out.println("Short wirting to file" + sData.toString());
            short tempData[] = Arrays.copyOf(sData, sData.length);
            try {
                // writes the data to file from buffer
                // stores the voice buffer
                byte bData[] = short2byte(sData);
                os.write(bData, 0, BufferElements2Rec * BytesPerElement);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //code for drawing a speaker on an action bar
            drawOnActionBar(tempData);

            //Code for countdown and automatic stop voice record
            countDown_automaticStop(tempData);

            pcmToAac();

        }
    }

    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    private void closePcmToAac(){
        try {
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mux.stop();
        mux.release();
    }

    private void initPcmToAac(){
        try {

            // 1 Setting the path
            // 2 Create and configure the MediaCodec object
            inputFile = new File(filePath);
            fis = new FileInputStream(inputFile);

            outputFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + COMPRESSED_AUDIO_FILE_NAME);
            if (outputFile.exists()) outputFile.delete();

            mux = new MediaMuxer(outputFile.getAbsolutePath(), MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4); //(output file, format)

            outputFormat = MediaFormat.createAudioFormat(COMPRESSED_AUDIO_FILE_MIME_TYPE, SAMPLING_RATE, 1); //(mime, sampleRate, channelCount)
            outputFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC); //(String, int)
            outputFormat.setInteger(MediaFormat.KEY_BIT_RATE, COMPRESSED_AUDIO_FILE_BIT_RATE); //(String, int) -> (bits/sec, 128000)

            codec = MediaCodec.createEncoderByType(COMPRESSED_AUDIO_FILE_MIME_TYPE);//(The desired mime type of the output data.)
            codec.configure(outputFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);//(MediaFormat format, Surface surface, MediaCrypto crypto, int flags)
            codec.start();

            codecInputBuffers = codec.getInputBuffers(); // Note: Array of buffers //Retrieve the set of input buffers. Call this after start() returns. After calling this method, any ByteBuffers previously returned by an earlier call to this method MUST no longer be used.
            codecOutputBuffers = codec.getOutputBuffers(); //Retrieve the set of output buffers. Call this after start() returns and whenever dequeueOutputBuffer signals an output buffer change by returning INFO_OUTPUT_BUFFERS_CHANGED. After calling this method, any ByteBuffers previously returned by an earlier call to this method MUST no longer be used.

            outBuffInfo = new MediaCodec.BufferInfo();//Per b
            }
        catch (FileNotFoundException e) {
            Log.e(LOGTAG, "File not found!", e);
        }
        catch (IOException e) {
            Log.e(LOGTAG, "IO exception!", e);
        }

    }

    private void pcmToAac(){

        try {

            byte[] tempBuffer = new byte[BUFFER_SIZE];
            boolean hasMoreData = true;
            double presentationTimeUs = 0;
            int audioTrackIdx = 0;
            int totalBytesRead = 0;
            int percentComplete;

            // 3 Loop until done:
            do {

                int inputBufIndex = 0;
                // 3.1 if an input buffer is ready:
                //     read a chunk of input, copy it into the buffer
                while (inputBufIndex != -1 && hasMoreData) {
                    inputBufIndex = codec.dequeueInputBuffer(CODEC_TIMEOUT_IN_MS);
                    // The timeout in microseconds, a negative timeout indicates "infinite".
                    // Returns the index of an input buffer to be filled with valid data or -1 if no such buffer is currently available

                    if (inputBufIndex >= 0) {
                        ByteBuffer dstBuf = codecInputBuffers[inputBufIndex];  // assigns a reference from the codecInputBuffers[inputBufIndex] byte array to the dstBuf
                        dstBuf.clear();// Clears this buffer.While the content of this buffer is not changed, the following internal changes take place: the current position is reset back to the start of the buffer, the value of the buffer limit is made equal to the capacity and mark is cleared.

                        int bytesRead = fis.read(tempBuffer, 0, dstBuf.limit()); //public int read (byte[] buffer, int byteOffset, int byteCount) - Reads up to byteCount bytes from this stream and stores them in the byte array buffer starting at byteOffset. Returns the number of bytes actually read or -1 if the end of the stream has been reached.

                        if (bytesRead == -1) { // -1 implies EOS
                            hasMoreData = false;
                            codec.queueInputBuffer(inputBufIndex, 0, 0, (long) presentationTimeUs, MediaCodec.BUFFER_FLAG_END_OF_STREAM);
                            // After filling a range of the input buffer at the specified index submit it to the component.
                            // (int index, int offset, int size, long presentationTimeUs, int flags)
                            // final piece of data
                        } else {
                            totalBytesRead += bytesRead;
                            dstBuf.put(tempBuffer, 0, bytesRead); //(byte[] src, int srcOffset, int byteCount)
                            codec.queueInputBuffer(inputBufIndex, 0, bytesRead, (long) presentationTimeUs, 0);
                            presentationTimeUs = 1000000l * (totalBytesRead / 2) / SAMPLING_RATE;  //?? presentationTimeUs could be 0
                        }
                    }
                }

                //3.2 if an output buffer is ready:
                //      copy the output from the buffer

                // Drain audio
                int outputBufIndex = 0;
                while (outputBufIndex != MediaCodec.INFO_TRY_AGAIN_LATER){  /*indicates that the call timed out. */

                    outputBufIndex = codec.dequeueOutputBuffer(outBuffInfo, CODEC_TIMEOUT_IN_MS); //Dequeue an output buffer, block at most "timeoutUs" microseconds. Returns the index of an output buffer that has been successfully decoded or one of the INFO_* constants.
                    if (outputBufIndex >= 0) {
                        //?? Are these lines needed:
//                                ByteBuffer encodedData = codecOutputBuffers[outputBufIndex]; //?? is this line needed
//                                encodedData.position(outBuffInfo.offset);  //Sets the position of this buffer. //The start-offset of the data in the buffer. //?? is this line needed
//                                encodedData.limit(outBuffInfo.offset + outBuffInfo.size);  //Sets the limit of this buffer.  //The start-offset of the data in the buffer. +  The amount of data (in bytes) in the buffer. //?? is this line needed
                        // --- ??

                        if ((outBuffInfo.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0 && outBuffInfo.size != 0) {  //outBuffInfo.flags = Buffer flags associated with the buffer. A combination of BUFFER_FLAG_KEY_FRAME and BUFFER_FLAG_END_OF_STREAM. MediaCodec.BUFFER_FLAG_CODEC_CONFIG = This indicated that the buffer marked as such contains codec initialization / codec specific data instead of media data.)
                            // Only the first time this code is going to run
                            // ?? What is the point of this line, is it needed
//                                    codec.releaseOutputBuffer(outputBufIndex, false); //if you are done with a buffer, use this call to return the buffer to the codec or to render it on the output surface.
                            // ---??
                        } else {
                            mux.writeSampleData(audioTrackIdx, codecOutputBuffers[outputBufIndex], outBuffInfo);  //Writes an encoded sample into the muxer.
                            codec.releaseOutputBuffer(outputBufIndex, false); //if you are done with a buffer, use this call to return the buffer to the codec or to render it on the output surface.
                        }

                    } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {  //The output format has changed, subsequent data will follow the new format. getOutputFormat() returns the new format.
                        outputFormat = codec.getOutputFormat();
                        Log.v(LOGTAG, "Output format changed - " + outputFormat);
                        audioTrackIdx = mux.addTrack(outputFormat);
                        mux.start();
                    } else if (outputBufIndex == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {  //The output buffers have changed, the client must refer to the new set of output buffers returned by getOutputBuffers() from this point on.
                        Log.e(LOGTAG, "Output buffers changed during encode!");
                    } else if (outputBufIndex == MediaCodec.INFO_TRY_AGAIN_LATER) {  //indicates that the call timed out.
                        // NO OP
                    } else {
                        Log.e(LOGTAG, "Unknown return code from dequeueOutputBuffer - " + outputBufIndex);
                    }
                }
//                percentComplete = (int) Math.round(((float) totalBytesRead / (float) inputFile.length()) * 100.0);
//                Log.v(LOGTAG, "Conversion % - "+ percentComplete);
            } while (outBuffInfo.flags != MediaCodec.BUFFER_FLAG_END_OF_STREAM && !mStop);

            Log.v(LOGTAG, "Compression done ...");
        } catch (FileNotFoundException e) {
            Log.e(LOGTAG, "File not found!", e);
        } catch (IOException e) {
            Log.e(LOGTAG, "IO exception!", e);
        }
    }

    //Code for countdown and automatic stop voice record
    private void countDown_automaticStop(short tempData1[]) {
        int sumFrame = 0;
        if(VoiceNrExced < silenceThreshold_1) {  //initially was 4000
            for (short s : tempData1) {
                if (s > silenceThreshold_1) {
                    VoiceNrExced = s;
                }
            }
        }
        else{
            sumFrame = 0;
            for (short r : tempData1) {
                sumFrame = sumFrame + (Math.abs(r));
            }
            if(automatics && j==0)  // j is for: This text cannot be shown when there is silence, for that we need to
            // show this "Window will close automatically ( " + seconds-- + " seconds )"
            {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(R.string.textview_autoclose);
                    }
                });
            }

//                Log.d("SEBA", "sum frame /1024 "+sumFrame/1024);

            if((sumFrame/BufferElements2Rec) < silenceThreshold_1 && automatics){  // Automatics is false when text is inserted
                j++;
                if(j==(NrFramesIn1Sec*frameMultiplyer)){ //44 frames = approximately one second
                    frameMultiplyer = frameMultiplyer+1;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(getString(R.string.textview_autoclose2_1) + " ( " + seconds-- + getString(R.string.seconds)+" )");
                        }
                    });
                }
            }
            else
            {
                // Reset counters because there is no more silence
                j=0;
                frameMultiplyer=1;
                seconds = inputSeconds;
            }

            if(j==NrFramesSilence && automatics){  // automatics - check if editext was used to enter text
                stopRecording();
                Log.d("SEBA", "Stopped ");

                if(isEmpty(editText)){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            exitActivity();
                        }
                    });
                }
//                break;
            }
        }
    }

    //code for drawing a speaker on an action bar
    private void drawOnActionBar(short tempData1[]) {

        int sumFrame = 0;
        for(short s : tempData1){
            sumFrame = sumFrame + (Math.abs(s));
        }

        if ((sumFrame/BufferElements2Rec) > silenceThreshold_3)
        {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    actionBar.setCustomView(v4);

                }
            });
        }else if ((sumFrame/BufferElements2Rec) < silenceThreshold_3 && (sumFrame/BufferElements2Rec) > silenceThreshold_2)
        {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    actionBar.setCustomView(v3);

                }
            });
        }else if((sumFrame/BufferElements2Rec) < silenceThreshold_2 && (sumFrame/BufferElements2Rec) > silenceThreshold_1) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    actionBar.setCustomView(v2);

                }
            });
        }else if((sumFrame/BufferElements2Rec) < silenceThreshold_1) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    actionBar.setCustomView(v1);

                }
            });
        }
    }

    private void stopRecording() {
        VoiceNrExced = 0;

        // stops the recording activity
        if (null != recorder) {
            isRecording = false;
            recorder.stop();
            recorder.release();
            recorder = null;
        }
        else Log.d("SEBA", "AudioRecorder = null");
    }

    public void doSomethingRadioB(View view){

//        txtViewSecCntdown.setVisibility(View.INVISIBLE);

        if(view.getId() == R.id.radioButtonSound){
            selection = RadioButtonsSound.WITH_SOUND;
            rb1.setTextColor(Color.parseColor("#000000"));
            rb2.setTextColor(Color.parseColor("#000000"));

        }
        else{
            selection = RadioButtonsSound.WITHOUT_SOUND;
        }
    }

    private boolean isEmpty(EditText etText) {
        return etText.getText().toString().trim().length() == 0;
    }

    private void delFile(final String s) {

        File file = new File(s);
        boolean deleted = file.delete();
    }

    private void  exitActivity(){
        if (automatics){
            stopRecording();
//            pcmToAac(); // converts a pcm file into aac
//            delFile(filePathAudioRec); //after convert we don't need a .pcm file
            finish();
        }
        else if (selection != RadioButtonsSound.SOUND_UNDECLARED){
            stopRecording();
//            pcmToAac(); // converts a pcm file into aac
//            delFile(filePathAudioRec); //after convert we don't need a .pcm file
            finish();
        }else{
            Toast toast = null;
            toast = Toast.makeText(getApplicationContext(), R.string.textview_select, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, -200);
            toast.show();

            //Pokaz ce je to v redu
            rb1.setTextColor(Color.parseColor("#B80000"));
            rb2.setTextColor(Color.parseColor("#B80000"));
        }
    }

    // Done button in Landscape mode
//    @Override
//    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//        if(i == EditorInfo.IME_ACTION_DONE)
//        {
//            savedText = editText.getText().toString();
////            Toast.makeText(this,"IME_ACTION_DONE works", Toast.LENGTH_SHORT).show();
//        }
//
//        return false;
//    }

    public void setNrFramesSilenceInSec(int seconds){
        // 44100 / 1024 = cca. 44
        NrFramesSilence = seconds * NrFramesIn1Sec;
        this.seconds = seconds;
        inputSeconds = seconds;
    }

    private enum RadioButtonsSound{
        WITHOUT_SOUND, WITH_SOUND, SOUND_UNDECLARED
    }

}
