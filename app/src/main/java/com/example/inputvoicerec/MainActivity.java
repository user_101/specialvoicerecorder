package com.example.inputvoicerec;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * This program is still not finished.
 * This program records audio and can also take notes in text form.
 * After the activity starts the recording of Audio starts and records all the time.
 * If the text is entered into EditText, before exiting the application the choice about
 * keeping or deleting audio has to be made.
 * The prorgram shuts down automatically if the user stops speaking and there is no text in the
 * EdiText box.
 * This program has the function that converts .pcm to aac.
 *
 *
 * */


public class MainActivity extends Activity {

    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        b1 = (Button) findViewById(R.id.button1);
    }

    public void doSomething(View v){
        Intent i = new Intent(this, ActivityB.class);
        startActivity(i);

    }
}
